use horrorshow::prelude::*;
use parsers;

pub fn page(content: Box<Render>) -> String {
    let tmp = html! {
        html {
            head {
                title { : "Subtitle Fixer" }
                meta(charset="utf-8");
                meta(description="Online tool to shift, stretch or crop subtitles. This tool can fix subtitles that are offset by a fixed amount or have a different frame rate, or both.");
                script(src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js") {
                }
                link(href="//fonts.googleapis.com/css?family=Bangers&subset=latin", rel="stylesheet", type="text/css") {
                }
                style {
                    :"td { padding: 10px }
                      tbody.error td {
                          border-right: solid 5px red;
                      }
                      body {
                          padding-bottom: 60px;
                      }
                      .submit_button {
                          font-size: 30px;
                          padding: 20px;
                          border-radius: 10px;
                          background-color: #444444;
                          color: white;
                          width: 200px;
                          margin: 20px;
                          margin-left: 0px;
                          text-align: center;
                          cursor: pointer;
                      }
                      .submit_button:hover {
                          color: yellow;
                      }
                      .footer {
                        position: fixed;
                        bottom: 0px;
                        background-color: white;
                        width: 100%;
                        height: 60px;
                        color: black;
                        opacity: 0.8;
                      }
                      .footer img {
                        background-color: white;
                        height: 60px;
                        float:  right;
                        margin-left: 10px;
                        margin-right: 10px;
                      }
                      .footer div {
                          margin-top: 21px;
                          width: 155px;
                          float: right;
                      }
                      p {
                          font-size: 25px;
                          color: gray;
                          letter-spacing: 2px;
                      }
                      h2 {
                          font-size: 60px;
                          color: #ff5722;
                      }
                      td {
                          font-size: 30px;
                      }
                      input { font-family: Bangers; font-size: 30px; }
                      input.button { display: block; margin: 10px; padding: 10px; font-family:Bangers;;}
                     .error-container { width: 100%; margin-top: 10px; margin-bottom: 10px; border: solid 1px #6f0202; padding: 10px; font-size: 30px;display: none;color:red; } 
                     .bottom-border { border-bottom: solid 1px gray; color: #ffeb3b; letter-spacing: 2px; }"
                }
            }
            body(style="position: relative; margin: 10px; background-color: black;color: white; font-family: Bangers, verdana;") {
                div(style="text-align:center;") {
                    h2 {
                        :"Subtitle Fixer" 
                    }
                }
                div {
                    :content
                }
                div(class="footer") {
                    img(src="http://z-petal.com/rust-logo-blk.svg");
                    div {
                        :"Made with ";
                        a(href="https://www.rust-lang.org/", target="_BLANK") {
                            :"Rust"
                        }
                        :" by ";
                        a(href="http://z-petal.com/about.html", target="_BLANK") {
                            :"Sandeep"
                        }
                    }
                }
            }
        }
    };
    tmp.into_string().unwrap()
}

pub fn form_page(fields: Box<Render>) -> String {
    page(box_html! {
                script {
                    : raw!("function extract_index(a) {
                        var parts = a.split('_');
                        if (parts.length === 2) {
                            return parseInt(parts[1]);
                        } else {
                            return NaN;
                        }
                       }

                       function validate() {
                           show_error('');
                           $('.error').removeClass('error');
                           var filled = get_filled_indexs(document);
                           var seen = [];
                           valid = true;
                           var fc = 0;
                           for (var k in filled) {
                               if (filled.hasOwnProperty(k)) {
                                   fc ++;
                                   var index = extract_index(k);
                                   if (seen.indexOf(index) === -1) {
                                       if (!validate_index(index, filled)) {
                                           valid = false;
                                       }
                                       seen.push(index);
                                   }
                               }
                           }
                           if (fc === 0) {
                               show_error('Please enter actual times of two dialogs (preferably from near the start and end of the movie).');
                               return false;
                           }
                           return valid;
                       }

                       function show_error(error) {
                           $('#error-container').text(error);
                           if (error === '') {
                               $('#error-container').hide();
                           } else {
                               $('#error-container').show();
                           }
                       }

                       function sub_val(cv, v) {
                           if (['hr_', 'mn_', 'sec_'].indexOf(cv) !== -1) {
                               return (v>=0 && v < 60);
                           } else if (cv === 'msec_') {
                               return (v>=0 && v < 1000);
                           } else {
                               return false;
                           }
                       }

                       function validate_index(index, filled) {
                           var fn = function(pv, cv) {
                               var v = parseInt(filled[cv + index]);
                               return (pv && !isNaN(v) && sub_val(cv, v));
                           };
                           var valid =  ['hr_', 'mn_', 'sec_', 'msec_'].reduce(fn, true);
                           if (!valid) {
                               $('#inputs_' + index).addClass('error').get(0).scrollIntoView();
                               show_error('Incomplete/Invalid inputs');
                           };
                           return valid;
                       }

                       function get_filled_indexs(root) {
                           var ret = {};
                           $(root).find('input[type=text]').each(function(i) {
                               if (this.value !== '') {
                                   ret[this.name] = this.value;
                               }
                           });
                           return ret;
                       }
                        ");
                }
                p {
                    p {
                    :"The time at which each of the dialog appear in this subtitle is shown below.";
                    }
                    p {
                    :"Select any two dialogs, preferably from the start and end of the movie, and enter the actual time they appear in the movie. The fixed .srt file will start downloading as soon as you submit the form.";
                    }
                }
                form(method="POST", enctype="multipart/form-data", action="", onsubmit="return validate()") {
                    : &fields
                }
            })
}

pub fn srt_fields(srt_file_name: String, original_name: String, ordered_dialogs: Vec<(i64, Vec<String>, Vec<String>)>) -> Box<Render> {
    box_html!{
                    div(id="error-container", class="error-container") {
                    }
                    input(type="hidden", name="srt_file_name", value=&srt_file_name);
                    input(type="hidden", name="original_name", value=&original_name);
                    div(class="submit_button", onclick="if (validate()) document.forms[0].submit()") {
                        :"Fix subtitles!"
                    }
                    div(style="background-color:#1d1d1d; height: 600px; width: 80%; overflow: auto;") {
                        table(style="width:100%", cellspacing="0", cellpadding="0") {
                            @for (index, dialog_time_components, dialogs) in ordered_dialogs.clone() {
                                tbody (id=format_args!("inputs_{}", index)) {
                                    tr {
                                        td  {
                                            input(type="text", placeholder=&dialog_time_components[0], size="2", name=format_args!("hr_{}", index));
                                            :":";
                                            input(type="text", placeholder=&dialog_time_components[1], size="2", name=format_args!("mn_{}", index));
                                            :":";
                                            input(type="text", placeholder=&dialog_time_components[2], size="2", name=format_args!("sec_{}", index));
                                            :",";
                                            input(type="text", placeholder=&dialog_time_components[3], size="3", name=format_args!("msec_{}", index));
                                        }
                                    }
                                    tr {
                                        td(class="bottom-border") {
                                            @for dialog in dialogs {
                                                :dialog;
                                                br;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    div(class="submit_button", onclick="if (validate()) document.forms[0].submit()") {
                        :"Fix subtitles!"
                    }
                }
}

pub fn index(maybe_error: Option<String>) -> String {
    let mut error = "".to_string();
    if let Some(e) = maybe_error {
        error = e;
    }
    page(box_html! {
        @if error != "" {
            div(id="error-container", class="error-container", style="display: block") {
                :&error
            }
        }
        p {
            p {
            :"This is a program that can sync movie subtitles to the copy of the movie you have in your possession.
            This can fix situations when the subtitles are offset by a fixed amount or when the movie's
            frame rate differs from the subtitle's frame rate, or both !";
            }

            p {
                :"By using this, you can even use subtitles made for 2 or 3 cds with one file or the other way around!"
            }

            p {
            :"To start, select the .srt file you want to fix";
            }
        }
        form(method="POST", enctype="multipart/form-data", action="") {
            input(type="file", name="srt_file", class="button", style="padding-top:50px;font-size: 50px;");
            div(class="submit_button", onclick="document.forms[0].submit()") {
                :"Upload subtitle!"
            }
        }
    })
}
