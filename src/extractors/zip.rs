use std::io::Result as IOResult;
use std::fs::File;
use std::io::{Error, ErrorKind};
use zip::read::ZipArchive;
use std::path::PathBuf;
use std::io::Read;
use std::io::Write;
use errors::{self, HastilyResult};

pub fn extract(src: &PathBuf, dst: &PathBuf) -> HastilyResult<Vec<PathBuf>> {
    let mut extracted_files: Vec<PathBuf> = Vec::new();
    let input_file = match File::open(src) {
        Ok(file) => file,
        Err(_) => return errors::make_error("Error in opening zip file".to_string()),
    };

    let mut archive = match ZipArchive::new(input_file) {
        Ok(archive) => archive,
        Err(_) => return errors::make_error("Cannot read zip archive".to_string()),
    };

    for i in 0..archive.len() {
        let mut file = archive.by_index(i).unwrap();
        println!("Filename: {}", file.name());
        let mut dst_dir: PathBuf = dst.clone();
        dst_dir.push(file.name());
        let mut out_file = match File::create(&dst_dir) {
            Ok(out_file) => out_file,
            Err(err) => {
                println!("{}", err);
                return errors::make_error("Cannot create output file".to_string());
            }
        };
        let mut content: Vec<u8> = Vec::new();
        match file.read_to_end(&mut content) {
            Err(_) => {
                println!("Cannot read archive");
            }
            _ => {}
        }
        match out_file.write_all(&content) {
            Err(_) => {
                println!("Cannot write extracted file");
            }
            _ => {
                extracted_files.push(dst_dir.clone());
            }
        }
    }

    Ok(extracted_files)
}
