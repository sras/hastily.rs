use parsers::srt::{Srt, Dialog, DialogTime, duration_to_ms, convert_duration_to_string};
use std::time::Duration;
use std::fmt::{self, Display};

pub enum Sign {
    positive,
    negative
}

impl Display for Sign {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Sign::positive => {
                write!(f, "+");
            },
            Sign::negative => {
                write!(f, "-");
            }
        }
        return Ok(());
    }
}

pub fn fix(mut srt: &mut Srt, ref_start: Dialog, ref_end: Dialog) -> (Option<f64>, Option<(Duration, Sign)>) {
    let multipler = stretch(&mut srt, &ref_start, &ref_end);
    let offset = correct_fixed_offset(&mut srt, &ref_start);
    return (multipler, offset);
}

pub fn correct_fixed_offset(srt: &mut Srt, ref_start: &Dialog) -> Option<(Duration, Sign)> {
    let target = match srt.subtitles.iter().position(|x| *x == *ref_start) {
        Some(index) => {
            srt.subtitles[index].clone()
        },
        None => {
            panic!("Cannot find start reference dialog");
        }
    };
    if ref_start.time.start_time > target.time.start_time {
        let offset = ref_start.time.start_time.clone() - target.time.start_time.clone();
        for dialog in &mut srt.subtitles {
            dialog.time.start_time = dialog.time.start_time + offset;
            dialog.time.end_time = dialog.time.end_time + offset;
        }
        return Some((offset, Sign::positive));
    } else {
        let offset = target.time.start_time.clone() - ref_start.time.start_time.clone();
        let mut offsetted_subs: Vec<Dialog> = Vec::new();
        for dialog in &mut srt.subtitles {
            if dialog.time.start_time >= offset {
                let mut fixed_dialog = dialog.clone();
                fixed_dialog.time.start_time = dialog.time.start_time - offset;
                fixed_dialog.time.end_time = dialog.time.end_time - offset;
                fixed_dialog.index = (offsetted_subs.len() as i64) + 1;
                offsetted_subs.push(fixed_dialog);
            }
        }
        srt.subtitles = offsetted_subs;
        return Some((offset, Sign::negative));
    }
    return None;
}

pub fn stretch(srt: &mut Srt, ref_start: &Dialog, ref_end: &Dialog) -> Option<f64> {
    let real_interval = ref_end.time.clone() - ref_start.time.clone();
    let mut sub_start: Option<Dialog> = None;
    let mut sub_end: Option<Dialog> = None;
    for dialog in &srt.subtitles {
        if *dialog == *ref_start {
            sub_start = Some(dialog.clone());
        }
        if *dialog == *ref_end {
            sub_end = Some(dialog.clone());
        }
    }

    match (sub_start, sub_end) {
        (Some(sub_start_dialog), Some(sub_end_dialog)) => {
            let sub_interval = sub_end_dialog.time - sub_start_dialog.time;
            let real_ms = duration_to_ms(real_interval) as f64;
            let sub_ms = duration_to_ms(sub_interval) as f64;
            let time_multiplier:f64 = real_ms/sub_ms;
            for dialog in &mut srt.subtitles {
                dialog.time = dialog.time.clone() * time_multiplier;
            }
            return Some(time_multiplier);
        },
        _ => {
            return None;
        }
    }
}
