extern crate rustc_serialize;
use self::rustc_serialize::json;
use self::rustc_serialize::json::DecodeResult;

use hyper::header::Connection;
use hyper::Client;
use hyper::Url;
use std::io::Read;
use std::collections::HashMap;

use movie_info::MovieInfo;
use errors::{self, HastilyResult};

type OmdbMovieInfo = HashMap<String, String>;

#[derive(RustcDecodable)]
pub struct OmdbSearchResult {
    Search: Vec<OmdbMovieInfo>,
    totalResults: String,
    Response: String,
}

fn make_query_for_slug(slug: String) -> Option<Url> {
    match Url::parse("http://www.omdbapi.com") {
        Ok(mut omdb_url) => {
            omdb_url.set_query_from_pairs(vec![("s", slug), ("r", "json".to_string())]);
            return Some(omdb_url);
        }
        Err(_) => {
            return None;
        }
    }
}

fn make_movie_info(omdb_movie_info: OmdbMovieInfo) -> MovieInfo {
    let empty = "".to_string();
    return MovieInfo {
        title: omdb_movie_info.get("Title").unwrap_or(&empty).to_string(),
        year: omdb_movie_info.get("Year").unwrap_or(&empty).to_string(),
        movie_type: omdb_movie_info.get("Type").unwrap_or(&empty).to_string(),
        imdb: omdb_movie_info.get("imdbID").unwrap_or(&empty).to_string(),
    };
}

pub fn search_slug(movie_name_slug: String) -> HastilyResult<Vec<MovieInfo>> {
    let url = make_query_for_slug(movie_name_slug).expect("Cannot make search query url");
    let client = Client::new();
    let mut res = try!(client.get(url).header(Connection::close()).send());
    let mut body = String::new();
    res.read_to_string(&mut body).unwrap();
    let search_result: OmdbSearchResult = try!(json::decode(&body));
    let mut results = Vec::new() as Vec<MovieInfo>;
    for omdb_movie_info in search_result.Search {
        results.push(make_movie_info(omdb_movie_info));
    }
    return Ok(results);
}
