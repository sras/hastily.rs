use regex::Regex;
use std::str::FromStr;
use std::io;
use std::fs::File;
use std::io::BufRead;
use std::fmt::{self, Display};
use std::cmp::PartialEq;
use std::path::PathBuf;

use std::time::Duration;
use std::hash::Hash;
use std::hash::Hasher;
use std::ops::Sub;
use std::ops::Mul;

pub struct DialogTime {
    pub start_time: Duration,
    pub end_time: Duration,
}

impl Display for DialogTime {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "{} --> {}",
               convert_duration_to_string(self.start_time),
               convert_duration_to_string(self.end_time))
    }
}

impl Mul<f64> for DialogTime {
    type Output = DialogTime;
    fn mul(self, rhs: f64) -> DialogTime {
        let mut start_ms = duration_to_ms(self.start_time) as f64;
        let mut end_ms = duration_to_ms(self.end_time) as f64;
        start_ms = start_ms * rhs;
        end_ms = end_ms * rhs;
        return DialogTime {
            start_time: Duration::from_millis(start_ms as u64),
            end_time: Duration::from_millis(end_ms as u64)
        };
    }
}

#[derive(Debug)]
pub struct DialogParseError;

impl FromStr for DialogTime {
    type Err = DialogParseError;
    fn from_str(s: &str) -> Result<DialogTime, DialogParseError> {
        let parts: Vec<&str> = s.split("-->").map(|x| x.trim()).collect();
        if parts.len() < 2 {
            return Err(DialogParseError);
        }
        let duration_start_parts: Vec<&str> = parts[0].split(":").collect();
        let duration_end_parts: Vec<&str> = parts[1].split(":").collect();
        match (convert_to_ms(duration_start_parts), convert_to_ms(duration_end_parts))  {
            (Some(start_ms), Some(end_ms)) => {
                return Ok(DialogTime {
                    start_time: Duration::from_millis(start_ms),
                    end_time: Duration::from_millis(end_ms),
                });
            },
            _ => {
                return Err(DialogParseError);
            }
        }
    }
}

impl Clone for DialogTime {
    fn clone(&self) -> DialogTime {
        return DialogTime { start_time: self.start_time.clone(), end_time: self.end_time.clone() };
    }
}

impl Sub<DialogTime> for DialogTime {
    type Output = Duration;

    fn sub(self, rhs: DialogTime) -> Duration {
        return self.start_time - rhs.start_time;
    }
}

impl Mul<(u32, u32)> for DialogTime {
    type Output = DialogTime;

    fn mul(self, ratio:(u32, u32)) -> DialogTime {
        let mut output = self.clone();
        let (num, deno) = ratio;
        output.start_time = (output.start_time * num)/deno;
        output.end_time = (output.end_time * num)/deno;
        return output;
    }
}

fn convert_to_ms(p: Vec<&str>) -> Option<u64> {
    if p.len() != 3 {
        return None;
    } else {
        match (p[0].parse::<u64>(), p[1].parse::<u64>()) {
            (Ok(hours), Ok(mins)) => {
                let seconds_part: Vec<&str> = p[2].split(",").collect();
                match (seconds_part[0].parse::<u64>(), seconds_part[1].parse::<u64>()) {
                    (Ok(seconds), Ok(milliseconds)) => {
                        return Some(hours * 3600_000 + mins * 60_000 + seconds * 1000 + milliseconds);
                    },
                    _ => {
                        return None;
                    }
                }
            },
            _ => {
                return None;
            }
        }
    }
}


pub fn convert_duration_to_components(duration: Duration) -> Vec<String> {
    let seconds = duration.as_secs();
    let hours = seconds.wrapping_div(3600);
    let rem = seconds.wrapping_rem(3600);
    let minutes = rem.wrapping_div(60);
    let seconds = rem.wrapping_rem(60);
    let ms: u64 = duration.subsec_nanos() as u64 / 1_000_000;
    return vec!(format!("{:0>#02}", hours), format!("{:0>#02}", minutes), format!("{:0>#02}", seconds), format!("{:0>#03}", ms));
}

pub fn convert_duration_to_string(duration: Duration) -> String {
    let components = convert_duration_to_components(duration);
    return format!("{}:{}:{},{}",
                   components[0],
                   components[1],
                   components[2],
                   components[3]);
}

pub struct Dialog {
    pub index: i64,
    pub time: DialogTime,
    pub dialogs: Vec<String>,
    hash: String
}

impl Dialog {
    pub fn new(index: i64, time: DialogTime, dialogs: Vec<String>) -> Dialog {
        let mut lc_dialogs: Vec<String> = Vec::new();
        for d in &dialogs {
            lc_dialogs.push(d.to_lowercase());
        }
        return Dialog { index: index, time: time, dialogs: dialogs.clone(), hash: lc_dialogs.concat() };
    }

    pub fn len(&self) -> usize {
        let mut r: usize = 0;
        for d in &self.dialogs {
            r = r + d.len();
        }
        return r;
    }

    pub fn contains_any(&self, targets: &Vec<&str>) -> bool {
        for t in targets {
            if self.hash.contains(&t.to_lowercase()) {
                return true;
            }
        }
        return false;
    }
}

impl PartialEq for Dialog {
    fn eq(&self, other: &Dialog) -> bool {
        self.hash == other.hash
    }
}

impl Eq for Dialog {
}

impl Hash for Dialog {
    fn hash<H: Hasher>(&self, state: &mut H) {
        state.write(self.hash.as_bytes());
    }
}

impl Clone for Dialog {
    fn clone(&self) -> Dialog {
        return Dialog::new(self.index.clone(), self.time.clone(), self.dialogs.clone())
    }
}

impl Display for Dialog {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}\n{}\n", self.index, self.time);
        for dialog in &self.dialogs {
            write!(f, "{}\n", dialog);
        }
        return Ok(());
    }
}

pub struct Srt {
    pub filename: Option<PathBuf>,
    pub subtitles: Vec<Dialog>
}

impl Clone for Srt {
    fn clone(&self) -> Srt {
        return Srt {filename: self.filename.clone(), subtitles: self.subtitles.clone()};
    }
}

impl Display for Srt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for subtitle in &self.subtitles {
            write!(f, "{}\n", subtitle);
        }
        return Ok(());
    }
}

fn get_one_dialog(input: &Vec<String>, mut offset: usize) -> Result<(Option<Dialog>, usize), ()> {
    if offset < input.len() {
        let index = input[offset].clone();
        offset += 1;
        let time = input[offset].clone();
        offset += 1;
        let mut dialogs: Vec<String> = Vec::new();
        let len = input.len();
        loop {
            if (offset == len) || (input[offset].trim() == "") {
                break;
            }
            dialogs.push(input[offset].clone());
            offset += 1;
        }
        let dialog = match index.parse() {
            Ok(int_index) => {
                match time.parse() {
                    Ok(dialog_time) => {
                        Dialog::new(int_index, dialog_time, dialogs)
                    },
                    _ => {
                        return Ok((None, offset + 1));
                    }
                }
            },
            Err(err) => {
                println!("Cannot parse index '{}'", index);
                return Ok((None, offset + 1));
            }
        };
        return Ok((Some(dialog), offset + 1));
    } else {
        return Err(());
    }
}

pub fn parse(srt_lines: String) -> Option<Srt> {
    let mut lines: Vec<String> = Vec::new();
    for line in srt_lines.lines() {
        lines.push(line.to_string());
    }
    let mut dialogs: Vec<Dialog> = Vec::new();
    let mut offset: usize = 0;
    loop {
        match get_one_dialog(&lines, offset) {
            Ok(r) => {
                match(r) {
                    (Some(dialog), new_offset) => {
                        dialogs.push(dialog);
                        offset = new_offset;
                    }
                    (None, new_offset) => {
                        offset = new_offset;
                    }
                }
            }
            _ => {
                if dialogs.len() > 0 {
                    return Some(Srt { subtitles: dialogs, filename: None });
                } else {
                    return None;
                }
            }
        }
    }
}

pub fn duration_to_ms(duration: Duration) -> u64 {
    return duration.as_secs() * 1000 + duration.subsec_nanos() as u64 / 1_000_000;
}
